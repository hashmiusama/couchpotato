var chat = {}

chat.fetchMessages = function(){
	$.ajax({
		url: 'ajax/chat.php',
		type: 'post',
		data: {method: 'fetch'},
		success: function(data){
			$('.chat .messages').html(data);
		}
			
	});	
}

chat.throwMessage = function(message){
	if($.trim(message).length != 0){
		$.ajax({
		url: 'ajax/chat.php',
		type: 'post',
		data: {method: 'throw', messages: message},
		success: function(data){
			chat.fetchMessages();
			$('.entry').val('');
			$('.messages').scrollTop($('.messages').prop("scrollHeight"));
		}	
	});	
	}
}

$(document).ready(function(e) {
   	$('.entry').keydown(function(e) {
        if(e.keyCode === 13 && e.shiftKey === false){
			chat.throwMessage($(this).val());
			e.preventDefault();
			$('.messages').scrollTop($('.messages').prop("scrollHeight"));
		}
    }); 
	$('.messages').scrollTop($('.messages').prop("scrollHeight"));
});


chat.interval = setInterval(chat.fetchMessages, 500);
chat.fetchMessages();