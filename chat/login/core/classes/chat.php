<?php
	class Chat extends Core{
		public function fetchMessages(){
			$this->query("
				SELECT `chat`.`message`,
						`users`.`username`,
						`users`.`id`
				FROM	`chat`
				JOIN 	`users`
				ON		`chat`.`user_id` = `users`.`id`
				ORDER BY `chat`.`timestamp`
				ASC
			");
			return $this->rows();
		}
		
		public function throwMessages($user_id, $message){
			$this->query("INSERT INTO `chat` (`message_id`,`user_id`,`message`,`timestamp`) VALUES ('','".(int)$user_id."','".$this->db->real_escape_string(htmlentities($message))."',UNIX_TIMESTAMP())");
		}
	}
?>